//
//  citySelector.swift
//  WeatherMe
//
//  Created by its on 27/11/2014.
//  Copyright (c) 2014 EC. All rights reserved.
//

import UIKit

class citySelector: UITableViewController {
    
  
    var cities = [cityModelNo]()
    var dre = []
    struct citySimple {
        var name: String
        var currtemp: Float
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getInitialCities()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.cities.count
        
        
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cityCell", forIndexPath: indexPath) as UITableViewCell
        
        if cities.count > 0 {
            let city = cities[indexPath.row]
            cell.textLabel?.text = city.name
            var st = String(format:"%.1f", city.initTemp)
            cell.detailTextLabel?.text = "Avg Temp: \(st) C"
        }else{
            println("notyhin")
            
        }
        

       // cell.textLabel?.text = "Dummy"
        
        //cell.detailTextLabel?.text = self.dummyTemp[indexPath.row]
        // Configure the cell...

        return cell
    }
    
    func getInitialCities(){
        WeatherInformer.FeedForCities({(returnedCities: Array<cityModelNo>) in
            self.cities = returnedCities
            
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()

            
            })
        
        })
        
    }
    
    
    // prepare segue function
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dest = segue.destinationViewController as? CityViewController {
            println("prepping for the city detail controller")
            let pathIndex = self.tableView.indexPathForSelectedRow()
            if let row:Int = pathIndex?.row {
                println("selected row: \(row)")
                let cityGot = cities[row] as cityModelNo
                dest.cityName = cityGot.name
                println("City Gotten Was: \(dest.cityName)")
            }

        }
        
        //try to get the row for the selected cell and map to the array
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
