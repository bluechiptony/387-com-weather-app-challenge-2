//
//  CityViewController.swift
//  WeatherMe
//
//  Created by Anthony Egwu on 28/11/2014.
//  Copyright (c) 2014 EC. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {

    
    var cityName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = self.cityName {
            println("city Name: \(name)")
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
