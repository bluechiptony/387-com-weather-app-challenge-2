//
//  WeatherInformer.swift
//  WeatherMe
//
//  Created by Anthony Egwu on 27/11/2014.
//  Copyright (c) 2014 EC. All rights reserved.
//

import UIKit


struct cityModel {
    var id: Int
    var name: String
    //var coordinates: Dictionary< String : Float >
    var country: String
}

struct cityModelNo {
    var id: Int
    var name: String
    
    var country: String
    var initTemp: Double
    
}

struct DayForecast {
    var date: Int
    var temp: temps
    var pressure: Double
    var humidity: Int
    var weather: weatherStruct
    
    var speed: Double
    var deg: Int
    var clouds: Int
    var rain: Int
}

struct temps {
    var day:Double
    var min:Double
    var max:Double
    var night:Double
    var eve: Double
    var morn: Double
    
    
}

struct weatherStruct {
    var id: Int
    var main: String
    var desc: String
    var icon: String
}
//define urls for different purposes

var urlForCities = "http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric"


class WeatherInformer{
    
    //function used to get city
    func getCity(){
        
    }
    
    class func FeedForCities(completion: (result:Array<cityModelNo>)->()){
        
        //define empty array for
        var cities = Array<cityModelNo>()
        var initStruct = cityModelNo(id: 0, name: "Get Forecast For Current Location", country: "", initTemp: 0)
                //process url
        var url = NSURL(string: urlForCities)
        var request = NSURLRequest(URL: url)
        var queue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: {(response: NSURLResponse!, data: NSData!, error: NSError!)-> Void in
            
            if(error != nil){
                println("API error")
            }else{
                println("No Api Error")
                
                var jsonError: NSError?
                var jsonDict: NSDictionary = NSJSONSerialization.JSONObjectWithData(data , options: NSJSONReadingOptions.MutableContainers, error: nil)as NSDictionary
                
                //check for json errors
                
                if jsonError != nil {
                    println("error Parsing Json Feed: \(jsonError)")
                }else{
                    if let cityDict: Array<AnyObject> = jsonDict["list"] as? Array<AnyObject> {
                        cities.append(initStruct)

                        //loop through feed to create city structs
                        for cit in cityDict{
                            var newCit = cityModelNo(id: 0, name: "", country: "", initTemp: 0.0)
                            if let citname:String = cit["name"] as? String {
                                newCit.name = citname
                                println(citname)
                            }
                            if let citId:Int = cit["id"] as? Int {
                                newCit.id = citId
                            }
                            if let citCon: String = cit["country"] as? String{
                                newCit.country = citCon
                                
                            }
                            if let citWe: NSDictionary = cit["main"] as? NSDictionary {
                                if let temp: Double = citWe["temp"] as? Double {
                                    newCit.initTemp = temp
                                }
                            }

                            
                            cities.append(newCit)
                        }
                    }
                }
            }
            completion(result: cities)
        })
        println("Where they at tho")
        
        //return cities
    }
    
    
    class func getforecastForCity(cityid: String, completion: (result: Array<DayForecast>)->() ){
        var urlForCity = "api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=xml&units=metric&cnt=7"
        
        //add city name to the url
        var urlForCityName = "http://api.openweathermap.org/data/2.5/forecast/daily?id=524901"
        
       // var urlForCityName = "http://api.openweathermap.org/data/2.5/forecast/daily?id=\(cityid)"
        //declasre array for forecast
        var Forecasts = Array<DayForecast>()
        
        var url = NSURL(string: urlForCityName)
        var request = NSURLRequest(URL: url)
        var queue = NSOperationQueue()
        
        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: {(response: NSURLResponse!, data: NSData!, error: NSError!)-> Void in
            
            if(error != nil){
                println("API error \(error)")
            }else{
                println("No Api Error")
                
                var jsonError: NSError?
                var jsonDict: NSDictionary = NSJSONSerialization.JSONObjectWithData(data , options: NSJSONReadingOptions.MutableContainers, error: nil)as NSDictionary
                
                //check for json errors
                
                if jsonError != nil {
                    println("error Parsing Json Feed: \(jsonError)")
                }else{
                    var dumWeather = weatherStruct(id: 0, main: "", desc: "", icon: "")
                    var dumTemp = temps(day: 0, min: 0, max: 0, night: 0, eve: 0, morn: 0)
                    var datForecast = DayForecast(date: 0, temp:dumTemp, pressure: 0, humidity: 0, weather: dumWeather, speed: 0, deg: 0, clouds: 0, rain: 0)
                    println(datForecast.date)
                    if let DayArray: Array<AnyObject> = jsonDict["list"] as? Array<AnyObject> {
                        for day in DayArray{
                            //start of main array loop
                            
                            //get the date integer
                            if let date:Int = day["dt"] as? Int {
                                datForecast.date = date
                                println(datForecast.date)
                                println(date)
                            }else{
                                println("none found")
                            }
                            
                            //get the temperatures struct struct
                            if let tempDict: NSDictionary = day["temp"] as? NSDictionary {
                                if let tday:Double = tempDict["day"] as? Double{
                                    dumTemp.day = tday
                                    println("day temperature: \(dumTemp.day)")
                                    println("-----------------------")
                                    
                                }else{
                                    println("not done")
                                }
                                
                                if let tmin:Double = tempDict["min"] as? Double{
                                    dumTemp.day = tmin
                                }
                                if let tmax:Double = tempDict["max"] as? Double{
                                    dumTemp.day = tmax
                                }
                                if let tnight:Double = tempDict["night"] as? Double{
                                    dumTemp.day = tnight
                                }
                                if let teve:Double = tempDict["eve"] as? Double{
                                    dumTemp.day = teve
                                }
                                if let tmorn:Double = tempDict["morn"] as? Double{
                                    dumTemp.day = tmorn
                                }
                                datForecast.temp = dumTemp
                            }
                            
                            //get the pressure
                            if let pressure:Double = day["pressure"] as? Double{
                                datForecast.pressure = pressure
                            }
                            
                            //get the humidity
                            
                            if let hum:Int = day["humidity"] as? Int{
                                datForecast.humidity = hum
                            }
                            
                            //get the reather details
                            
                            if let weDict: Array<AnyObject> = day["weather"] as? Array<AnyObject> {
                                println("array seen")
                                
                                for val in weDict{
                                    if let wid:Int = val["day"] as? Int{
                                        dumWeather.id = wid
                                        
                                    }else{
                                        println("not done")
                                    }
                                    
                                    if let weMain:String  = val["main"] as? String{
                                        dumWeather.main = weMain
                                    }
                                    
                                    if let Wedesc:String  = val["description"] as? String{
                                        dumWeather.desc = Wedesc
                                        println("Dum w is: \(dumWeather.desc)")
                                    }
                                    
                                    if let weicon:String  = val["icon"] as? String{
                                        dumWeather.icon = weicon
                                    }
                                    
                                }
                                datForecast.weather  = dumWeather
                                
                                
                            }else{
                                println("no dict")
                            }
                            
                            
                            
                            
                            //get the speed
                            if let speed:Double = day["pressure"] as? Double{
                                datForecast.speed = speed
                            }
                            
                            //get the degrees
                            
                            if let deg:Int = day["humidity"] as? Int{
                                datForecast.deg = deg
                            }
                            
                            //get the clouds
                            
                            if let clouds:Int = day["humidity"] as? Int{
                                datForecast.clouds = clouds
                            }
                            
                            
                            Forecasts.append(datForecast)
                            //end of array main loop
                        }
                        
                        
                    }else{
                        println("no array")
                    }
                }
            }
            //completion(result: Forecasts)
            for forc in Forecasts{
                println("Pressure is :\(forc.pressure)")
                
                var wede:weatherStruct = forc.weather
                var fw:String = wede.desc
                println("It Will Be \(fw) Today")
                
                
            }
            
        })


        
        
        
    }

   
}
