
import Foundation

struct BookSummary {
    var id:String
    var title:String
    var subTitle:String
    var thumbnail:String
}

struct BookDetail {
    var title:String
    var subTitle:String
    var description:String
    var thumbnail:String
}

class Booklist {
    
    class func getBooks(completion: (result:Array<BookSummary>)->()) {
        var bookArray = [BookSummary]()
        let url = NSURL(string: "https://www.googleapis.com/books/v1/volumes?q=iphone+programming&maxResults=40&projection=lite&fields=items(id,volumeInfo(title),volumeInfo(subtitle),volumeInfo(imageLinks(thumbnail)))")
       // if let tempUrl:NSURL = url {
            let urlReq = NSURLRequest(URL: url)
            let queue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if (error != nil) {
                    println("API error: \(error), \(error.userInfo)")
                }
                var jsonError:NSError?
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                if (jsonError != nil) {
                    println("Error parsing json: \(jsonError)")
                }
                else {
                    if let status:String = json["status"] as? String {
                        println("server status: \(status)")
                    }
                    var newBook = BookSummary(id: "", title: "", subTitle: "", thumbnail: "")
                    if let books:Array<AnyObject> = json["items"] as? Array<AnyObject> {
                        for book in books {
                            if let id = book["id"] as? String {
                                newBook.id = id
                            }
                            if let info = book["volumeInfo"] as? NSDictionary {
                                if let title = info["title"] as? String {
                                    newBook.title = title
                                }
                                if let subTitle = info["subtitle"] as? String {
                                    newBook.subTitle = subTitle
                                }
                                if let imageLink = info["imageLinks"] as? NSDictionary {
                                    if let thumbnail = imageLink["thumbnail"] as? String {
                                        newBook.thumbnail = thumbnail
                                    }
                                }
                            }
                            bookArray.append(newBook)
                        }
                    }
                }
                completion(result: bookArray)
            })
        //}
    }
    
    class func getBook(bookID:String, completion: (result:BookDetail)->()) {
        let url = NSURL(string: "https://www.googleapis.com/books/v1/volumes/\(bookID)?fields=volumeInfo(title),volumeInfo(subtitle),volumeInfo(description),volumeInfo(imageLinks(extraLarge))")
        //if let tempUrl = url {
            let urlReq = NSURLRequest(URL: url)
            let queue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if (error != nil) {
                    println("API error: \(error), \(error.userInfo)")
                }
                var jsonError:NSError?
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                var newBook = BookDetail(title: "", subTitle: "", description: "", thumbnail: "")
                if (jsonError != nil) {
                    println("Error parsing json: \(jsonError)")
                }
                else {
                    if let status:String = json["status"] as? String {
                    }
                    if let volumeInfo = json["volumeInfo"] as? NSDictionary {
                        if let title = volumeInfo["title"] as? String {
                            newBook.title = title
                        }
                        if let subTitle = volumeInfo["subtitle"] as? String {
                            newBook.subTitle = subTitle
                        }
                        if let description = volumeInfo["description"] as? String {
                            newBook.description = description
                        }
                        if let imageLinks = volumeInfo["imageLinks"] as? NSDictionary {
                            if let thumbnail = imageLinks["extraLarge"] as? String {
                                newBook.thumbnail = thumbnail
                            }
                        }
                    }
                }
                completion(result: newBook)
            })
        //}
    }
}